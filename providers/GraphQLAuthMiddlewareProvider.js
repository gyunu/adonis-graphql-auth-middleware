const { ServiceProvider } = require.main.require('@adonisjs/fold')

const auth = require('../src/auth')

class GraphQLAuthProvider extends ServiceProvider {
  register () {
    this.app.bind('Adonis/GraphQL/Middleware/Auth', () => auth)
  }
}

module.exports = GraphQLAuthProvider
