const auth = (requiredPermission) => (next) => async (root, args, { auth, request, ...rest }, info) => {
  const Config = use('Config')
  if (!Config.get('auth').enabled) {
    return next(root, args, { auth, request, ...rest }, info)
  }

  const valid = await auth.check()

  if (!valid) {
    throw new Error('Not Authenticated')
  }

  const Database = use('Database')
  const Token = use('App/Models/Token')

  const token = await Token.query().where('token', request.header('Authorization').split(' ')[1]).first()
  if (!token) {
    throw new Error('Invalid Token')
  }

  const user = await token.user().fetch()
  if (!user) {
    throw new Error('Invalid Token')
  }

  let userPermissions = []

  const roles = await user.roles().fetch()
  const permissions = await user.getPermissions()

  for (const role of roles.rows) {
    const perms = await role.getPermissions()
    userPermissions = userPermissions.concat(perms)
  }

  userPermissions = userPermissions.concat(permissions)

  if (userPermissions.indexOf(requiredPermission) === -1) {
    throw new Error('User does not have the required permission')
  }

  return next(root, args, { user, auth, request, ...rest }, info)
}

module.exports = auth
